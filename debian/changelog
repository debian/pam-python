pam-python (1.1.0~git20220701.1d4e111-0.6) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/patches:
    + Fix regression that re-introduced CVE-2019-16729 for Py>=3.12 builds.
      (Closes: #1076955).
    + Rename 1001_Py_DontWriteBytecodeFlag-deprecation-in-py312.patch to
      1001_Use-new-PyConfig-API-when-building-Python-3.12-any-beyond.patch.
    + Add 1002_src-Makefile-Don-t-error-out-on-aggregate-return-war.patch.
      Don't -Werror when -Waggregate-return gets triggered by the compiler.
  * debian/control:
    + Bump Standards-Version to 4.7.0. No changes needed.

 -- Mike Gabriel <mike.gabriel@das-netzwerkteam.de>  Sat, 24 Aug 2024 09:32:43 +0200

pam-python (1.1.0~git20220701.1d4e111-0.5) unstable; urgency=medium

  * debian/patches:
    + Add 1001_Py_DontWriteBytecodeFlag-deprecation-in-py312.patch.
      Use new PyConfig API for setting global config vars starting
      with Python 3.12. (Closes: #1061376).

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 07 Feb 2024 08:07:10 +0100

pam-python (1.1.0~git20220701.1d4e111-0.4) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix the path to the Python intersphinx repository. Closes: #1061304.

 -- Matthias Klose <doko@debian.org>  Mon, 22 Jan 2024 14:45:27 +0100

pam-python (1.1.0~git20220701.1d4e111-0.3) unstable; urgency=medium

  * debian/{copyright,control,watch}:
    + Switch to own (sunweaver) upstream location.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 23 Oct 2022 13:58:36 +0200

pam-python (1.1.0~git20220701.1d4e111-0.2) unstable; urgency=medium

  * Non-maintainer upload.

  * debian/tests/control:
    + Use python3-pam (not python-pam) test dependency.
    + Add 'libpam0g-dev | libpam-dev' to Depends: field.
  * debian/patches:
    + Amend autopkgtests. This adds
      0009_src-test-pam_python.pam.in-Use-more-tabs-make-file-m.patch,
      0010_src-Makefile-When-creating-symlink-test-pam_-PYTHON-.patch, and
      0011_src-Makefile-Fix-sed-expression-pam_python.so-is-spe.patch.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 23 Oct 2022 10:10:22 +0200

pam-python (1.1.0~git20220701.1d4e111-0.1) unstable; urgency=medium

  * Non-maintainer upload.

  * New Git snapshot from @Ionic providing Python3 support. (Closes: #937234).
  * debian/patches:
    + Add 000[2-8]_*.patch: Various patches from upstream Git (still pending
      submission). Rename intersphinx-localmapping.diff to
      2001_intersphinx-localmapping.patch.
    + Rebase 2001_intersphinx-localmapping.patch against upstream version bump
      patch.
  * debian/watch:
    + Obtain Git snapshot from gh/Ionic/pam-python, branch feature/py3.
  * debian/source.lintian-overrides:
    + Drop sourceforce related override.
  * debian/copyright:
    + Update/improve copyright attributions.
    + Use secure URL in Format: field.
  * debian/{compat,control}:
    + Switch to debhelper-compat notation and bump DH compat level to version
      13.
  * debian/rules (et al.):
    + Convert to pure debhelper.
  * debian/control:
    + Bump Standards-Version: to 4.6.1. No changes needed.
    + Re-phrase package SYNOPSIS and LONG_DESCRIPTION fields.
    + Add Vcs-*: fields. Maintain the packaging of this project on
      salsa.debian.org in the future.
    + Adjust layout of Depends: fields.
    + Add B-D: python3-pam (needed for unit tests, if one chooses to run
      them locally).
    + Fix unusual field spacing in Homepage: field.
    + Add Rules-Requires-Root: field and set it to 'no'.
  * debian/libpam-python-doc.links:
    + Fix notation (don't start a path with a slash).
    + Symlink to more .js files available in sphinx, libjs-underscore and
      libjs-jquery. (This probably needs more love).
  * debian/tests:
    + Re-enable autopkgtests (as a goal, should work, not-testable due to
      broken Debian sid atm).

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 23 Oct 2022 01:26:40 +0200

pam-python (1.0.9-2) unstable; urgency=high

  * Source only upload for migration to testing

 -- Russell Stuart <russell-debian@stuart.id.au>  Mon, 23 Nov 2020 18:57:51 +1000

pam-python (1.0.9-1) unstable; urgency=high

  * Get rid of tests.  They are python2 only.  Closes: 974589.

 -- Russell Stuart <russell-debian@stuart.id.au>  Fri, 13 Nov 2020 07:17:29 +1000

pam-python (1.0.8-1) unstable; urgency=high

  * New upstream.
  * Move to explicit python2 versioning.  Closes: #967194.

 -- Russell Stuart <russell-debian@stuart.id.au>  Thu,  5 Nov 2020 20:47:48 +1000

pam-python (1.0.7-1) unstable; urgency=high

  * New upstream.

 -- Russell Stuart <russell-debian@stuart.id.au>  Wed, 18 Sep 2019 20:25:13 +1000

pam-python (1.0.6-1) unstable; urgency=low

  * New upstream.
  * Add debian specific patch to link sphinx to python-doc instead
    of online version.  (Closes: #833411).
  * Bump standards version.

 -- Russell Stuart <russell-debian@stuart.id.au>  Sat, 27 Aug 2016 21:37:03 +1000

pam-python (1.0.5-1) unstable; urgency=low

  * New upstream.
  * Bump standards version.

 -- Russell Stuart <russell-debian@stuart.id.au>  Fri, 19 Feb 2016 20:51:53 +1000

pam-python (1.0.4-1) unstable; urgency=low

  * New upstream, moved to AGPL-3.0.
  * debian/rules now uses dpkg-buildflags to harden binary.
    (Closes: #744156).
  * Now ships examples.  (Closes: #686652).

 -- Russell Stuart <russell-debian@stuart.id.au>  Fri, 30 May 2014 06:31:46 +1000

pam-python (1.0.3-1) unstable; urgency=low

  * Fix lintian warnings under jessie.
  * New upstream, improving compatibility with older python versions.

 -- Russell Stuart <russell-debian@stuart.id.au>  Sun,  4 May 2014 23:30:32 +1000

pam-python (1.0.2-1) unstable; urgency=low

  * New upstream.
  * Bumped standards version to 3.9.3.

 -- Russell Stuart <russell-debian@stuart.id.au>  Thu,  5 Apr 2012 15:23:06 +1000

pam-python (1.0.1-1) unstable; urgency=low

  * New upstream incorporating --load-as-needed patch from Ubuntu.
    (Closes: #606700).
  * Removed Petter Reinholdtsen from Uploaders at his suggestion.

 -- Russell Stuart <russell-debian@stuart.id.au>  Mon, 13 Dec 2010 09:12:09 +1000

pam-python (1.0.0-2) unstable; urgency=low

  * debian/watch: fixed url.

 -- Russell Stuart <russell-debian@stuart.id.au>  Sun,  4 Jul 2010 09:55:20 +1000

pam-python (1.0.0-1) unstable; urgency=low

  * New upsteam release - documentation format changed to sphinx.
    (Closes: #582754).
  * Split into binary and doc packages because documentation now
    depends on libjs-jquery.
  * Switch to dpkg-source 3.0 (quilt) format

 -- Russell Stuart <russell-debian@stuart.id.au>  Tue, 29 Jun 2010 13:27:36 +1000

pam-python (0.1.1-2) unstable; urgency=low

  * Fix rules file to work properly when building arch dependent
    packages.
  * Correct clean target in the rules file to make sure unpatching is
    done after make clean is executed, to make sure the patches are in
    effect when cleaning is done.

 -- Petter Reinholdtsen <pere@debian.org>  Mon, 17 May 2010 16:57:49 +0200

pam-python (0.1.1-1) unstable; urgency=low

  * Initial release based on package from upstream (Closes: #578650).
  * Drop build depend on latex2html to avoid non-free dependency.
  * Upgrade from debhelper 5 to 7.
  * Update standards-version from 3.7.3 to 3.8.4.  No changes needed.

 -- Petter Reinholdtsen <pere@debian.org>  Wed, 05 May 2010 10:25:24 +0200
